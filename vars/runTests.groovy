#!/user/bin/env groovy
def call() {
    script {
        dir("app") {
            sh "npm install"
            sh "npm run test"
        }
    }
}