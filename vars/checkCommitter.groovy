#!/user/bin/env groovy
def call() {
    script {
        env.LAST_COMMITTER_EMAIL = sh(script: 'git log -1 --pretty=format:"%ae"', returnStdout: true).trim()
        if (env.LAST_COMMITTER_EMAIL == "jenkins@example.com") {
            currentBuild.result = 'SUCCESS'
            error("Build skipped as the last commit was made by Jenkins")
        }
    }
}