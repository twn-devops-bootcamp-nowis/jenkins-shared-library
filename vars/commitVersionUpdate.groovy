#!/user/bin/env groovy
def call() {
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
        dir("app") {
            sh 'git config --global user.email "jenkins@example.com"'
            sh 'git config --global user.name "jenkins"'
            sh 'git remote set-url origin https://$USER:$PASS@gitlab.com/twn-devops-bootcamp-nowis/jenkins-exercises-twnbcmod-8.git'
            sh 'git add .'
            sh 'git commit -m "ci: version bump"'
            sh 'git push origin HEAD:master'
        }
    }
}