#!/user/bin/env groovy
def call() {
    withCredentials([usernamePassword(credentialsId: 'docker-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
        script {
            sh "docker build -t ${env.IMAGE_NAME} ."
            sh 'echo $PASS | docker login -u $USER --password-stdin'
            sh "docker push ${env.IMAGE_NAME}"
        }
    }
}