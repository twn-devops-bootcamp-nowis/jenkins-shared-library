#!/user/bin/env groovy
def call() {
    script {
        dir("app") {
            sh "npm version patch"
            def packageJson = readJSON file: 'package.json'
            def version = packageJson.version
            env.IMAGE_NAME = "${env.DOCKER_REPO}:module8-${version}-${env.BUILD_NUMBER}"
        }
    }
}